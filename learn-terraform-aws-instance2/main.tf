terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"

  cloud {
    organization = "arthur"

    workspaces {
      name = "Example-Workspace"
    }
  }

}

provider "aws" {
  profile = "default"
  region  = "us-west-2"
}

data "aws_vpc" "selected" {
  id = "vpc-00b3d790fca3da129"
}

resource "aws_subnet" "my_subnet" {
  vpc_id            = data.aws_vpc.selected.id
  cidr_block        = "10.0.10.0/24"
  availability_zone = "us-west-2a"

  tags = {
    Name = "arthur"
  }
}

resource "aws_network_interface" "foo" {
  subnet_id   = aws_subnet.my_subnet.id
  private_ips = ["10.0.10.100"]

  tags = {
    Name = "arthur_primary_eth"
  }
}

resource "aws_instance" "app_server" {
  ami           = "ami-08d70e59c07c61a3a"
  instance_type = "t2.micro"

  network_interface {
    network_interface_id = aws_network_interface.foo.id
    device_index         = 0
  }

  tags = {
    Name = var.instance_name
  }
}
